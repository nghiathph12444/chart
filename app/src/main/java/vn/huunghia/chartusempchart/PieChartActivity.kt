package vn.huunghia.chartusempchart

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate

class PieChartActivity : AppCompatActivity() {
    var chart: PieChart? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pie_chart)
        chart = findViewById(R.id.pc_chart)

        setupPieChart()
        loadPieChartData()
    }

    private fun loadPieChartData(){
        val list: ArrayList<PieEntry> = ArrayList()
        list.add(PieEntry(0.3f, "Test"))
        list.add(PieEntry(0.5f, "Test 1"))
        list.add(PieEntry(0.2f, "test 2"))

        val listColor: ArrayList<Int> = ArrayList()
        listColor.add(Color.BLUE)
        listColor.add(Color.RED)
        listColor.add(Color.YELLOW)

        //danh sách note
        val dataset = PieDataSet(list, "Chart")
        dataset.colors = listColor

        val data = PieData(dataset)
        data.setDrawValues(true)
        data.setValueFormatter(PercentFormatter(chart))
        data.setValueTextSize(10f)
        data.setValueTextColor(Color.BLACK)

        chart?.data = data
        chart?.invalidate()

        //animation
        chart?.animateY(1500, Easing.EaseInOutQuad)
    }

    private fun setupPieChart(){
        chart?.setDrawSlicesUnderHole(true)
        chart?.setUsePercentValues(true)
        chart?.setEntryLabelTextSize(10f)
        chart?.setEntryLabelColor(Color.BLACK)
        //bỏ đục lỗ ở giữa hình tròn
        chart?.isDrawHoleEnabled = false
//        chart?.centerText = "Demo"
//        chart?.setCenterTextSize(20f)


        chart?.description?.isEnabled  = false

        //vị trí danh sách note
        val legend = chart?.legend
        legend?.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        legend?.horizontalAlignment = Legend.LegendHorizontalAlignment. RIGHT
        legend?.orientation = Legend.LegendOrientation.VERTICAL
        legend?.setDrawInside(false)
        legend?.isEnabled = true
    }
}